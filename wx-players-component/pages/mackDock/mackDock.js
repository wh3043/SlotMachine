// pages/mackDock/mackDock.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [1, 1, 2, 2, 2, 3, 0, 0, 0],
    more: false,
    iii: -1,
    elist: []
  },

  queryElist() {
    let query = wx.createSelectorQuery()
    query.selectAll(".item").boundingClientRect(function (rect) {
      // rect.id      // 节点的ID
      rect.dataset // 节点的dataset
      rect.left    // 节点的左边界坐标
      rect.right   // 节点的右边界坐标
      rect.top     // 节点的上边界坐标
      rect.bottom  // 节点的下边界坐标
      // rect.width   // 节点的宽度
      // rect.height  // 节点的高度
    })

    query.exec((res) => {
      // console.log(res)
      this.data.elist = res[0]
    })
  },

  bindtap(e) {
    let jjj = e.target.dataset.i
    if (jjj == this.data.iii)
      jjj = -1
    this.setData({ iii: jjj })
  },

  touchmove(e) {
    let x = e.changedTouches[0].pageX
    let y = e.changedTouches[0].pageY

    for (e of this.data.elist) {

      if (x > e.left && x < e.right && y > e.top && y < e.bottom) {
        let jjj = e.dataset.i
        if (jjj != this.data.iii) {
          this.setData({ iii: jjj })
          this.queryElist()
        }
      }
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.queryElist()
  },

})