import Machine from "./utils/machine.js"

Page({
  data: {
    // 老虎机
    num1: 0,
    num2: 0,
    num3: 0,
    num4: 0,
    speed:24,
    machine: {
      transY1: 0,
      transY2: 2,
      transY3: 1,
      transY4: 3,
      nums: [0,1,2,3,4,5,6,7,8,9,0]
    }
  },

  onLoad () {
    let that = this;
    this.machine = new Machine(this, {
      height: 40,  //单个数字高度
      len: this.data.machine.nums.length-1,  //数字个数   
      transY1: 0,
      num1: that.data.num1,
      transY2: 0,
      num2: that.data.num2,
      transY3: 0,
      num3: that.data.num3,
      transY4: 0,
      num4: that.data.num4,
      speed: that.data.speed,
      callback: () => {
        wx.hideLoading()
        wx.showModal({
          title: '提示',
          content: '恭喜您，中奖了',
          // showCancel: false,
          success: (res) =>  {
            // this.machine.reset()
              console.log(res)
            if (res.confirm) {
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          },          
        })          
      }      
    })

  },

  onReady () {
    // 老虎机
    let machine = this.data.machine
    for (const i in this.data.machine) {      
      if (typeof(machine[i]) == "number") machine[i] *= -40  //单个数字高度
    }    
    this.setData({machine})
  },

  onStart () {
    this.machine.start()
    wx.showLoading({mask: true})
  },
  numChage(e){
    let value = e.detail.value;
    let num = e.target.dataset.num
     if(num==1){
       this.setData({num1:value})
     }else if(num==2){
       this.setData({ num2: value })
     } else if (num == 3) {
       this.setData({ num3: value })
     } else if (num == 4) {
       this.setData({ num4: value })
     }
  },
  speedChange(e){
    let speed = e.detail.value;
    this.setData({speed});
  },

  navigate(e) {
    let url = e.target.dataset.url
    url = `/pages/${url}/${url}`
    wx.navigateTo({ url })
  }
})